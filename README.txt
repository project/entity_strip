
The Entity Strip module provides a drush command to print out database dump
configuration in order to strip tables related to specific entities from
a database dump.

Drush command
=============
* drush des

Print out necessary commands to strip drupal entity tables from a DB.

Examples
  Print exclusion lines for commerce_order and commerce_line_item entity tables.
   drush des commerce_order commerce_line_item

  Prints table names as a yml array with example_key as the array key to a file
  named entity_strip_example.yml.
    drush des commerce_order  commerce_line_item --format="yml"
      --parent-key="example_key"  --result-file="entity_strip_example.yml"

Arguments:
 entities - A space delimited list of entities.

Options:
  --force
      Whether or not to force entity fields to be excluded regardless of if they
      are on non-excluded entities. The efault is FALSE.
  --format
      The output format desired. The default si "txt".
  --result-file
      The name of the output filefile we want to output to. Will not display output in CI when set. Default - NULL
  --parent-key
      The desired parent key of the json or yml format. The default is
      "scrub_tables".
  --prefix
      The non-escaped string to place in front of table names in txt format. The
      default is
      "ADDITIONAL_EXCLUDE_DATA_FROM="${ADDITIONAL_EXCLUDE_DATA_FROM}|^">
  --suffix
      The escape string to place behind table names in txt format. The default
      is $\".

Aliases: des
