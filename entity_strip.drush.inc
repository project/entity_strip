<?php

/**
 * @file
 * Print out necessary commands to strip drupal entity tables from a DB.
 *
 * Args
 * entities - space separated list of entities to exclude.
 *
 * Options
 * - prefix - eg the 'EXCLUDE_DATA_FROM="${EXCLUDE_DATA_FROM}|^' portion of
 * the output.
 * - suffix - \n portion of the output.
 * - format - JSON, yml, txt, csv.
 * - parent-key - The parent key for the array.
 * - output-file - path to the output file.
 * - force - Forces the exclusion of fields
 *           regardless of if it's on another entity.
 */

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityNullStorage;
use Drupal\Core\Entity\ContentEntityStorageBase;

/**
 * Implements hook_drush_help().
 */
function entity_strip_drush_help($section) {
  switch ($section) {
    case 'drush:db_entity_strip':
    case 'drush:des':
      return dt('Print out necessary commands to strip drupal entity tables from a DB.');
  }
}

/**
 * Implements hook_drush_command().
 */
function entity_strip_drush_command() {
  $items = [];

  $items['db_entity_strip'] = [
    'callback' => 'entity_strip_exclude_tables',
    'description' => dt('Creates excludes for entity tables from db exports.'),
    'aliases' => ['des'],
    'arguments' => [
      'entities' => "A space delimited list of entities.",
    ],
    'options' => [
      'format' => 'The output format desired. Default - "txt"',
      'prefix' => 'The non-escaped string to place in front of table names in txt format. Default - ADDITIONAL_EXCLUDE_DATA_FROM="${ADDITIONAL_EXCLUDE_DATA_FROM}|^',
      'suffix' => 'The escape string to place behind table names in txt format. Default - $\"',
      'parent-key' => 'The desired parent key of the json or yml format. Default - "scrub_tables"',
      'output-file' => 'The file we want to output to. Will not display output in CI when set. Default - NULL',
      'force' => 'Whether or not to force entity fields to be excluded regardless of if they are on non-excluded entities. Default - FALSE',
    ],
    'examples' => [
      'drush des commerce_order commerce_line_item' => 'Prints exclusiong lines for commerce_order and commerce_line_item entity tables.',
      'drush des commerce_order commerce_line_item --format="yml" --parent-key="example_key" --output-file="entity_strip_example.yml"' => 'Prints table names as a yml array with example_key as the array key to a file named entity_strip_example.yml.',
    ],
  ];

  return $items;
}

/**
 * Excludes entity tables from db exports.
 *
 * @return bool
 *   Whether script was succesful or not.
 */
function entity_strip_exclude_tables() {
  $entities = drush_get_arguments();
  unset($entities[0]);

  $options = [
    'prefix' => $options['prefix'] ?? 'ADDITIONAL_EXCLUDE_DATA_FROM="${ADDITIONAL_EXCLUDE_DATA_FROM}|^',
    'suffix' => $options['suffix'] ?? "$\"",
    'format' => $options['format'] ?? "txt",
    'parent_key' => $options['parent-key'] ?? "scrub_tables",
    'force' => $options['force'] ?? FALSE,
    'output_file' => $options['output-file'],
  ];

  if (empty($entities)) {
    return drush_set_error('ENTITY_STRIP_ENTITIES_NOT_PASSED_IN', dt('You must provide a space separated list of entities to exclude.'));
  }

  $tables = [];
  foreach ($entities as $entity_type) {
    $entity_type_manager = Drupal::entityTypeManager();
    try {
      $definition = $entity_type_manager->getDefinition($entity_type);
    }
    catch (PluginNotFoundException $e) {
      // @todo: Print a nice error message?.
      continue;
    }
    $storage = $entity_type_manager->getStorage($entity_type);

    if (!$storage instanceof ContentEntityNullStorage) {
      // Exclude entity's tables.
      if ($definition->getBaseTable()) {
        $tables[] = $definition->getBaseTable();
      }

      if ($definition->getRevisionTable()) {
        $tables[] = $definition->getRevisionTable();
      }

      if ($definition->getDataTable()) {
        $tables[] = $definition->getDataTable();
      }

      if ($definition->getRevisionDataTable()) {
        $tables[] = $definition->getRevisionDataTable();
      }

      if ($storage instanceof ContentEntityStorageBase) {
        /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
        $table_mapping = $storage->getTableMapping();
        if ($table_mapping) {
          $all_entity_tables = $table_mapping->getTableNames();
          $tmp = array_merge($tables, $all_entity_tables);
          $tables = array_unique($tmp);
        }
      }
    }
  }

  $result = entity_strip_to_string($tables, $options);
  if (empty($options['output_file'])) {
    print $result;
  }

  return TRUE;
}


/**
 * Print out array of tables in the format desired.
 *
 * @param array $tables
 *   The array of tables we are excluding.
 * @param array $options
 *   An array of options passed into the script.
 *
 * @return string
 *   The final result of the script in the format desired.
 */
function entity_strip_to_string(array $tables = [], array $options = []) {
  $string = '';

  switch ($options['format']) {
    case 'txt':
      foreach ($tables as $table) {
        $string .= $options['prefix'] . $table . $options['suffix'] . "\n";
      }
      break;

    case 'csv':
      $string = implode(',', $tables);
      break;

    case 'yml':
      $string = $options['parent_key'] . ":\n";
      foreach ($tables as $table) {
        $string .= '  - ' . $table . "\n";
      }
      break;

    case 'json':
      $tables = [$options['parent_key'] => $tables];
      $string = json_encode($tables, JSON_PRETTY_PRINT) . "\n";
      break;
  }

  if (!empty($options['output_file'])) {
    $fp = fopen($options['output_file'], 'w') or die("Unable to open file!");
    fwrite($fp, $string);
    fclose($fp);
  }

  return $string;
}
