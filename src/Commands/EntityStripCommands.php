<?php

namespace Drupal\entity_strip\Commands;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityNullStorage;
use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Entity strip drush command.
 */
class EntityStripCommands extends DrushCommands {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MigrateUpgradeCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Outputs configuration to strip tables related to entities from db dump.
   *
   * @param string $entities
   *   A space delimited list of entities.
   * @param array $options
   *   An associative array of options whose values come from cli,
   *   aliases, config, etc.
   *
   * @option format
   *   The output format desired. The default is "txt".
   * @option prefix
   *   The non-escaped string to place in front of table names in txt format.
   * Default - ADDITIONAL_EXCLUDE_DATA_FROM="${ADDITIONAL_EXCLUDE_DATA_FROM}|^
   * @option suffix
   *   The escape string to place after table names. Default is $\".
   * @option parent-key
   *   The desired parent key of the json or yml format. The default is
   * "scrub_tables".
   * @option output-file
   *   The name of the output file. The default is NULL.
   * @option force
   *   Whether or not to force entity fields to be excluded regardless of if
   * they are on non-excluded entities. The default is FALSE.
   *
   * @usage drush des commerce_order commerce_line_item
   *   Prints exclusion lines for commerce_order and commerce_line_item entity
   * tables.
   * @usage drush des commerce_order commerce_line_item --format="yml" --parent-key="example_key" --output-file="entity_strip_example.yml"
   *   Prints table names as a yml array with example_key as the array key to a
   * file named entity_strip_example.yml.
   * @usage drush entity-strip des node
   *   Exclude all node tables.
   *
   * @command entity-strip:des
   *
   * @validate-module-enabled entity_strip
   *
   * @aliases des,db_entity_strip
   *
   * @return bool
   *   Indicator of success.
   *
   * @throws \Exception
   *   When an error occurs.
   */
  public function dbEntityStrip($entities, array $options = [
    'format' => NULL,
    'prefix' => NULL,
    'suffix' => NULL,
    'parent-key' => NULL,
    'output-file' => NULL,
    'force' => NULL,
  ]) {
    if (!is_array($entities)) {
      $entities = [$entities];
    }

    $options = [
      'prefix' => $options['prefix'] ?? 'ADDITIONAL_EXCLUDE_DATA_FROM="${ADDITIONAL_EXCLUDE_DATA_FROM}|^',
      'suffix' => $options['suffix'] ?? "$\"",
      'format' => $options['format'] ?? "txt",
      'parent_key' => $options['parent-key'] ?? "scrub_tables",
      'force' => $options['force'] ?? FALSE,
      'output_file' => $options['output-file'],
    ];

    if (empty($entities)) {
      return drush_set_error('ENTITY_STRIP_ENTITIES_NOT_PASSED_IN', dt('You must provide a space separated list of entities to exclude.'));
    }

    $tables = [];
    foreach ($entities as $entity_type) {
      $entity_type_manager = \Drupal::entityTypeManager();
      try {
        $definition = $entity_type_manager->getDefinition($entity_type);
      }
      catch (PluginNotFoundException $e) {
        // @todo: Print a nice error message?.
        continue;
      }

      $storage = $this->entityTypeManager->getStorage($entity_type);

      if (!$storage instanceof ContentEntityNullStorage) {
        // Exclude entity's tables.
        if ($definition->getBaseTable()) {
          $tables[] = $definition->getBaseTable();
        }

        if ($definition->getRevisionTable()) {
          $tables[] = $definition->getRevisionTable();
        }

        if ($definition->getDataTable()) {
          $tables[] = $definition->getDataTable();
        }

        if ($definition->getRevisionDataTable()) {
          $tables[] = $definition->getRevisionDataTable();
        }

        if (($storage instanceof ContentEntityStorageBase) && (!$storage instanceof ContentEntityNullStorage)) {
          /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
          $table_mapping = $storage->getTableMapping();
          if ($table_mapping) {
            $all_entity_tables = $table_mapping->getTableNames();
            $tmp = array_merge($tables, $all_entity_tables);
            $tables = array_unique($tmp);
          }
        }
      }
    }

    $result = $this->entityStripToString($tables, $options);
    if (empty($options['output_file'])) {
      print $result;
    }

    return TRUE;
  }

  /**
   * Print out array of tables in the format desired.
   *
   * @param array $tables
   *   The array of tables we are excluding.
   * @param array $options
   *   An array of options passed into the script.
   *
   * @return string
   *   The final result of the script in the format desired.
   */
  protected function entityStripToString(array $tables = [], array $options = []) {
    $string = '';

    switch ($options['format']) {
      case 'txt':
        foreach ($tables as $table) {
          $string .= $options['prefix'] . $table . $options['suffix'] . "\n";
        }
        break;

      case 'csv':
        $string = implode(',', $tables);
        break;

      case 'yml':
        $string = $options['parent_key'] . ":\n";
        foreach ($tables as $table) {
          $string .= '  - ' . $table . "\n";
        }
        break;

      case 'json':
        $tables = [$options['parent_key'] => $tables];
        $string = json_encode($tables, JSON_PRETTY_PRINT) . "\n";
        break;
    }

    if (!empty($options['output_file'])) {
      $fp = fopen($options['output_file'], 'w') or die("Unable to open file!");
      fwrite($fp, $string);
      fclose($fp);
    }

    return $string;
  }

}
